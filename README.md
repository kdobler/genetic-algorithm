# Genetic Algorithm

## Genetic Algorithm Application to Graph Based Pattern Recognition

This repository contains an implementation of a genetic algorithm to find a permutation matrix P that minimizes the following cost between the adjacency matrix A1 of graph g1 and the adjacency matrix A2 of g2:
$$ || A^T_1P - PA^T_2 ||^2_2 $$