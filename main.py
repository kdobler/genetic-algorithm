import os
import networkx as nx
import numpy as np
import random

from typing import List, Tuple
from utils import load_all_graphs, draw_all_graphs

def compute_adjacency_matrix(g: nx.Graph) -> np.ndarray:
    """
    Computes the adjacency matric of a graph

    Args:
        g: graph
    
    Returns:
        Adjacency matrix of the given graph
    """
    sorted_nodes = sorted(g.nodes, reverse = False)
    values = np.arange(0, len(sorted_nodes))
    nodes_dic = {k:v for k,v in zip(sorted_nodes, values)}
    A = np.zeros(shape=(len(g.nodes), len(g.nodes)), dtype=int)
    for (node1, node2) in g.edges:
        A[nodes_dic[node1], nodes_dic[node2]] = 1
        A[nodes_dic[node2], nodes_dic[node1]] = 1
    return A


def fitness_func(A_1: np.ndarray, A_2: np.ndarray, P: np.ndarray) -> float:
    """
    Compute the fitness score F(P) = ||A_1P - PA_2||_2^2

    Args:
        A_1: Adjacency matrix of graph 1
        A_2: Adjacency matrix of graph 2
        P: Permutation matrix

    Returns:
        Fitness score of the given permutation matrix

    """
    return np.linalg.norm(np.matmul(A_1, P) - np.matmul(P, A_2), ord=2) ** 2


class GeneticAlgorithm:

    def __init__(self, 
                 g_1: nx.Graph, 
                 g_2: nx.Graph, 
                 A_1: np.ndarray, 
                 A_2: np.ndarray, 
                 N: int, 
                 top_score: int, 
                 n_crossover: int,
                 n_mutation: int) -> None:
        
        self.g_1 = g_1
        self.g_2 = g_2
        self.A_1 = A_1
        self.A_2 = A_2
        self.N = N
        self.top_score = top_score
        self.n_crossover = n_crossover
        self.n_mutation = n_mutation
    
    def __str__(self):
        print(f'============== INITIALIZATION ==============')
        print(f'Initial population size = {self.N}')
        print(f'Number of top-matrices selected during each iterations = {self.top_score}')
        print(f'Number of random crossover (PMC) = {self.n_crossover}')
        print(f'Number of random mutations (EM) = {self.n_mutation}')
        print('============== RESULTS =====================')

    def create_population(self):
        """
        Creates the initial population of matrices.
        """
        P_size = self.A_1.shape[0]
        P = np.identity(n=P_size)
        population = [np.random.permutation(P) for n in range(self.N)]
        return population

    def evaluate_fitness(self, A_1: np.ndarray, A_2: np.ndarray, population) -> int:
        """
        Evaluates fitness of each individual in the population.
        """
        fitness_scores = {idx:fitness_func(A_1, A_2, population[idx]) for idx in range(len(population))}
        return fitness_scores
    
    def selection(self, fitness_scores: dict, population):
        """
        Selects top-individuals to breed.
        """
        sorted_fitness_scores = dict(sorted(fitness_scores.items(), key=lambda x:x[1]))
        idx = list(sorted_fitness_scores.keys())[:self.top_score]
        next_population = [population[i] for i in idx]
        return next_population
    
    def recurse_chr_1(self, candidate, exchanged_part_chr_1, exchanged_part_chr_2):
        """
        Fills the first chromosome recursively.
            In a nutshell, a candidate row is presented to the exchanged row(s), the one(s) coming from the other chromosome.
            If the candidate row is not present in the exchanged row(s), then no need to recurse., which means this row is unique.
            If the candidate row is present in the exchanged row(s), then recurse until you find a non-redundant row.
        """
        if candidate not in exchanged_part_chr_1:
            return candidate
        else:
            idx = list(exchanged_part_chr_1).index(candidate)
            exchanged_part_chr_1_popped = exchanged_part_chr_1.pop(idx)
            exchanged_part_chr_2_popped = exchanged_part_chr_2.pop(idx)
            return self.recurse_chr_1(exchanged_part_chr_2_popped, exchanged_part_chr_1, exchanged_part_chr_2)

    def recurse_chr_2(self, candidate, exchanged_part_chr_1, exchanged_part_chr_2):
        """
        Fills the second chromosome recursively.
        """
        if candidate not in exchanged_part_chr_2:
            return candidate
        else:
            idx = list(exchanged_part_chr_2).index(candidate)
            exchanged_part_chr_1_popped = exchanged_part_chr_1.pop(idx)
            exchanged_part_chr_2_popped = exchanged_part_chr_2.pop(idx)
            return self.recurse_chr_2(exchanged_part_chr_1_popped, exchanged_part_chr_1, exchanged_part_chr_2)
    
    def crossover(self, selected_population):
        """
        Computes Partially Mapped Crossover.
        """        
        new_offsprings = []

        for _ in range(self.n_crossover):
            
            # randomly select two permutation matrices
            rand_p = np.random.randint(0, len(selected_population), size=2)
            
            # flatten the two selected matrices into two chromosomes
            # i.e chr_1 = [(1,0,0), (0,1,0), (0,0,1)] -> if it's a 3-square diagonal matrix
            chr_1 = [tuple(row) for row in selected_population[rand_p[0]]]
            chr_2 = [tuple(row) for row in selected_population[rand_p[1]]]
            
            # get a list of possible breakpoint indices:
            # i.e if chr_1 = [(1,0,0), (0,1,0), (0,0,1)] -> possible_breakpoints = [0, 3, 6, 9]
            possible_breakpoints = np.arange(0, self.A_1.shape[0]*self.A_2.shape[0] + 1, self.A_1.shape[0])
            # randomly selects two breakpoints
            # break_point_1 must be sampled before the last possible breakpoint!
            # break_point_2 must be sampled from (but not including) break_point_1!
            break_point_1 = np.random.choice(possible_breakpoints[:-1])
            break_point_2 = np.random.choice(possible_breakpoints[list(possible_breakpoints).index(break_point_1) + 1:])
            
            # creates two empty chromosomes that will be filled later
            # i.e new_chr_1 = [(0,0,0), (0,0,0), (0,0,0)] -> if it's a 3-square matrix
            new_chr_1 = [tuple([0.0]*self.A_1.shape[0]) for _ in range(self.A_1.shape[0])]
            new_chr_2 = [tuple([0.0]*self.A_1.shape[0]) for _ in range(self.A_1.shape[0])]
            
            # get the index of the first breakpoint
            part_to_exchange = list(possible_breakpoints).index(break_point_1)
            # get how many rows must be exchanged
            number_of_parts = list(possible_breakpoints).index(break_point_2) - list(possible_breakpoints).index(break_point_1)

            # exchange the corresponding rows
            new_chr_1[part_to_exchange:part_to_exchange + number_of_parts] = chr_2[part_to_exchange:part_to_exchange + number_of_parts]
            new_chr_2[part_to_exchange:part_to_exchange + number_of_parts] = chr_1[part_to_exchange:part_to_exchange + number_of_parts]
            
            # loop through each item (=row) in the chromosomes and fill the part that are empty (with zeros only)            
            for x in range(len(new_chr_1)):

                # initiliaze because they are popped during recursion
                exchanged_part_chr_1 = chr_2[part_to_exchange:part_to_exchange + number_of_parts]
                exchanged_part_chr_2 = chr_1[part_to_exchange:part_to_exchange + number_of_parts]
                
                # copy to refill the second chromosomes because during recursion we pop one element from the list
                exchanged_part_chr_1_copy = exchanged_part_chr_1.copy()
                exchanged_part_chr_2_copy = exchanged_part_chr_2.copy()
                
                if sum(new_chr_1[x]) == 0: # if the item contains only zeros, it needs to be processed
                    new_chr_1[x] = self.recurse_chr_1(chr_1[x], exchanged_part_chr_1, exchanged_part_chr_2)

                if sum(new_chr_2[x]) == 0:
                    new_chr_2[x] = self.recurse_chr_2(chr_2[x], exchanged_part_chr_1_copy, exchanged_part_chr_2_copy)

            # reshape the chromosomes into matrices and append them to the new population
            new_offsprings.append(np.asarray(chr_1))
            new_offsprings.append(np.asarray(chr_2))
        
        return new_offsprings
        
    def mutation(self, selected_population):
        """
        Mutates individuals in the population using Exchange Mutation operator.
        """
        for _ in range(self.n_mutation):
            
            # randomly select one permutation matrix
            rand_p = np.random.randint(0, len(selected_population), size=1)

            # randomly select two rows from the matrix
            rand_rows = np.random.randint(0, self.A_1.shape[0], size=2)
            
            # exchange the two rows
            selected_population[rand_p[0]][[rand_rows[0], rand_rows[1]]] = selected_population[rand_p[0]][[rand_rows[1], rand_rows[0]]]
            
        return selected_population

    def run(self) -> Tuple[float, np.ndarray]:
        """
        Run the genetic algorithm to find the isomoprhism (permutation matrix P)
        between the adjacency matrices of graph 1 and graph 2.

        Args:
            A_1: Adjacency matrix of graph 1
            A_2: Adjacency matrix of graph 2

        Returns:
            A tuple with the fitness score of the best permutation matrix and the corresponding permutation matrix.
        """
        self.__str__()
        criterion = 4.0
        best_fitness = np.inf
        n_iter = 1000

        population = self.create_population()
        fitness = self.evaluate_fitness(self.A_1, self.A_2, population)
        
        for _ in range(n_iter):
            selected_population = self.selection(fitness, population)
            crossover_population = self.crossover(selected_population)
            mutated_population = self.mutation(crossover_population)
            new_fitness = self.evaluate_fitness(self.A_1, self.A_2, mutated_population)
            best_fitness = min(fitness.values())
            if best_fitness <= criterion:
                best_p = mutated_population[np.argmin(fitness)]
                print(f'The criterion was reached after {_} iterations')
                return best_fitness, best_p
            population = mutated_population
            fitness = new_fitness
        
        print('The number of iterations was reached and no permuation matrix satifying the criterion was found...')
        return best_fitness, 0


def main():
    # Set random seed
    np.random.seed(42)

    # 1. load the data
    graphs = load_all_graphs('./Exercise-3/data')

    # 2. Run the Genetic algorithm on the adjacency matrices of the data
    # 2.1 Tweak the parameters of the genetic algorithm to obtain a fitness score <= 4.0
    A_1, A_2 = compute_adjacency_matrix(graphs[0]), compute_adjacency_matrix(graphs[1])
    GA = GeneticAlgorithm(graphs[0], graphs[1], A_1, A_2, 1000, 10, 25, 25)
    res, P = GA.run()

    print(f'Best fitness value = {res}')
    print(P)
    print('Check if the matrix is a permutation matrix (should be doubly stochastic):')
    print(f'Row-wise sum = {np.sum(P, axis=1)}')
    print(f'Column-wise sum = {np.sum(P, axis=0)}')



if __name__ == '__main__':
    main()